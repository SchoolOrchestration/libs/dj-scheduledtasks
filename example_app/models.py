from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation
from scheduledtasks.models import ScheduledTask

TODO_STATUSES = [
    ('N', 'Todo'),
    ('P', 'In Progress'),
    ('D', 'Done'),
]

class Todo(models.Model):

    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=30)
    status = models.CharField(max_length=30, choices=TODO_STATUSES, default='N')
    users = models.ManyToManyField(get_user_model(), blank=True, related_name='shares')

    scheduled_tasks = GenericRelation(ScheduledTask)

    size = models.PositiveIntegerField(default=1)
    due = models.DateField(auto_now_add=True)
    due_time = models.DateTimeField(auto_now_add=True)
