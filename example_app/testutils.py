from scheduledtasks.models import ScheduledTask
from datetime import datetime
from scheduledtasks.models import ScheduledTask
import pytz

def create_scheduled_tasks(task_name, utc_timestrings):
    # '2018-06-29 17:08'
    tasks = []
    for date_time_str in utc_timestrings:

        date_time_obj = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M')
        date_time_obj = pytz.utc.localize(date_time_obj)
        task = ScheduledTask.schedule(
            task = task_name,
            time = date_time_obj
        )
        tasks.append(task)
    return tasks